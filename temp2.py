# -*- coding: utf-8 -*-

import random, curses

BOARD_SIZE_Y = 16
BOARD_SIZE_X = 16

K_UP = "w"
K_DOWN = "s"
K_LEFT = "a"
K_RIGHT = "d"
K_EXIT = "q"

T_EMPTY = " "
T_HOR_BORDER = "-"
T_VER_BORDER = "|"
T_HEAD = "@"
T_BODY = "x"
T_APPLE = "a"

def spawnApple(board):
    empties = []
    
    for y in range(BOARD_SIZE_Y):
        for x in range(BOARD_SIZE_X):
            if board[y][x] == T_EMPTY:
                empties.append([y, x])
    
    rI = random.choice(empties)
    
    sY = rI[0]
    sX = rI[1]
    
    board[sY][sX] = T_APPLE

def main(stdscr):
    board = []
    snek = []
    
    y_border = "\n" + T_VER_BORDER
    
    for y in range(BOARD_SIZE_Y):
        yArr = []
        y_border += T_HOR_BORDER
        
        for x in range(BOARD_SIZE_X):
            yArr.append(T_EMPTY)
        
        board.append(yArr)
    
    y_border += T_VER_BORDER
    
    posY = random.randrange(1, BOARD_SIZE_Y - 1)
    posX = random.randrange(1, BOARD_SIZE_X - 1)
    
    board[posY][posX] = T_HEAD
    
    spawnApple(board)
    
    move_dir = K_RIGHT
        
    alive = True
    won = False
    
    stdscr.timeout(500)
    
    while alive and not won:
        if len(snek) - 1 == BOARD_SIZE_X * BOARD_SIZE_Y:
            won = True
        
        stdscr.clear()
        
        out = ""
        out += y_border
        
        for y in range(BOARD_SIZE_Y):
            line = "\n" + T_VER_BORDER
            
            for x in range(BOARD_SIZE_X):
                line += board[y][x]
            
            line += T_VER_BORDER
            
            out += line
        
        out += y_border
        
        stdscr.addstr(out)
        stdscr.refresh()
        
        try:
            npt = stdscr.getkey()
        except:
            npt = None
        
        if npt in [K_UP, K_DOWN, K_LEFT, K_RIGHT]:
            move_dir = npt
        elif npt in [K_EXIT]:
            alive = False
            break
        
        oldPosY = posY
        oldPosX = posX
        
        if move_dir == K_UP:
            posY -= 1
        elif move_dir == K_DOWN:
            posY += 1
        elif move_dir == K_LEFT:
            posX -= 1
        elif move_dir == K_RIGHT:
            posX += 1
        
        if (posY < 0 or posY >= BOARD_SIZE_Y
            or posX < 0 or posX >= BOARD_SIZE_X):
            alive = False
            break
        
        spawnNewApple = False
        
        next_t = T_EMPTY
        
        if board[posY][posX] == T_BODY:
            alive = False
            break
        elif board[posY][posX] != T_APPLE and len(snek) > 0:
            lastSnek = snek.pop()
            board[lastSnek[0]][lastSnek[1]] = T_EMPTY
            next_t = T_BODY
        elif board[posY][posX] == T_APPLE:
            spawnNewApple = True
            next_t = T_BODY
        
        if len(snek) > 0 or next_t == T_BODY:
            snek.insert(0, [oldPosY, oldPosX])
        
        board[posY][posX] = T_HEAD
        board[oldPosY][oldPosX] = next_t
        
        if spawnNewApple:
            spawnApple(board)
    
    if not alive:
        print("died!")

curses.wrapper(main)
