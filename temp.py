# -*- coding: utf-8 -*-

import sys, time, random

#def progressBar(value, endvalue, bar_length=20):
#    percent = float(value) / endvalue
#    arrow = '-' * int(round(percent * bar_length)-1) + '>'
#    spaces = ' ' * (bar_length - len(arrow))
#
#    sys.stdout.write("\rPercent: [{0}] {1}%".format(arrow + spaces, int(round(percent * 100))))
#    sys.stdout.flush()
#
#progressBar(0.5, 1.0)
#time.sleep(1.0)
#progressBar(1.0, 1.0)

BOARD_SIZE_Y = 4
BOARD_SIZE_X = 4

board = []

y_border = "\n|"

for y in range(BOARD_SIZE_Y):
    yArr = []
    y_border += "-"
    
    for x in range(BOARD_SIZE_X):
        yArr.append(" ")
    
    board.append(yArr)

y_border += "|"

posY = random.randrange(0, BOARD_SIZE_Y)
posX = random.randrange(0, BOARD_SIZE_X)

board[posY][posX] = "@"

move_dir = "w"

alive = 0

while alive < 5:
    sys.stdout.flush()
    
    out = "\r"
    
    out += y_border
	
    for y in range(BOARD_SIZE_Y):
        line = "\n|"
        
        for x in range(BOARD_SIZE_X):
            line += board[y][x]
        
        line += "|"
        
        out += line
    
    out += y_border
    
    sys.stdout.write(out)
    
    if move_dir == "w":
        board[posY][posX] = " "
        posY -= 1
        board[posY][posX] = "@"
    
    alive += 1
